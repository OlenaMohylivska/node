const express = require('express');
const app = express();
const { checkStatic } = require('./helpers/index');

const {fileRouter} = require('./filesRouter');
const morgan = require('morgan');

app.use(morgan('tiny'));
app.use(express.json());
checkStatic();
app.use(fileRouter);

app.use((error, req, res, next) => {
  if(error) {
    res.status(500).json({
      message: "Server error"
    })
  }
})

app.listen(8080);