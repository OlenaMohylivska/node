const express = require('express');
const path = require('path');
const fsProm = require('fs').promises;
const fs = require('fs');
const moment = require('moment');
const router = express.Router();
const validExtensions = ['log', 'txt', 'json', 'yaml', 'xml', 'js'];

router.get('/api/files', (req, res) => {
  fs.readdir('./static', (err, items) => {
    if(err) {
      throw err;
    } else {
      res.status(200).json({
        message: 'Success',
        files: items
      })
    }  
  })
})

router.post('/api/files', (req, res) => {
  const { filename, content } = req.body;
  const regExp = /(.*)\.(.+)/;
 
  if (!filename || !content) {
    res.status(400).json({
      message: "Please specify 'content' parameter"
    })
  } else if(filename.length - 1 === filename.match(regExp)[2].length) {
    res.status(400).json({
      message: "Not appropriate file name"
    })
  } else if (!validExtensions.includes(filename.match(regExp)[2])) {
    res.status(400).json({
      message: 'Not appropriate file extensions'
    })
  } else {
    fs.stat(`./static/${filename}`, function (err, stats) {
      if (err) {
        fsProm.writeFile(`./static/${filename}`, content, 'utf-8');
        res.status(200).json({
          message: 'File created successfully'
        })
      } else {
        res.status(400).json({
          message: 'File already exist'
        })
      }
    });
  }
})

router.get(`/api/files/:filename`, (req, res) => {
  fs.stat(`./static/${req.params.filename}`, function (err, stats) {
    if (err) {
      res.status(400).json({
        message: `No file with ${req.params.filename} filename found`
      })
    } else {
      fs.readFile(`./static/${req.params.filename}`, 'utf-8', (err, content) => {
        if(err) {
          throw err;
        } else {
          res.status(200).json({
            message: "Success",
            filename: req.params.filename,
            content: content,
            extension: path.extname(req.params.filename),
            uploadedDate: moment().format().slice()
          })
        }
      })
    }
  });
})

router.delete(`/api/files/:filename`, (req, res) => {
  fs.stat(`./static/${req.params.filename}`, function (err, stats) {
    if (err) {
      res.status(400).json({
        message: `No file with ${req.params.filename} filename found`
      })
    } else {
      fs.unlink(`./static/${req.params.filename}`, (err, content) => {
        if(err) {
          throw err;
        } else {
          res.status(200).json({
            message: "Success"
          })
        }
      })
    }
  });
})

module.exports = {
  fileRouter: router
}